<?php
	/**
	* Login modal
	*/
?>
<div class="modal" data-modal-name="subscribe-form" aria-hidden="true" role="dialog" aria-labelby="subscribeTitle" aria-describedby="subscribeDescription">
	<div class="modal-container">
		<button data-modal-target="subscribe-form"><i class="fa fa-close"></i></button>
		<!--header>
			<h2 id="subscribeTitle"><?php _e('Tilaa uutiskirje');?></h2>
			<button data-modal-target="subscribe-form"><i class="fa fa-close"></i></button>
		</header>
		<p id="subscribeDescription"><?php _e('Täytä shköpostiosoitteesi, niin lisäämme sinut postituslistallemme.');?></p-->
		<?php
			if( function_exists( 'load_mailerlite_form' ) ) {
			    load_mailerlite_form(3);
			}
		?>

	</div>
</div>