
<section class="primary-content">
	<div class="container">
		<article name="e404" <?php post_class();?>>
			<h2><?php _e('Valitettavasti sisältöä ei löytynyt','dobbyts');?></h2>
			<p><?php _e('...Ei se mitään. Kokeile hakua tai valitse valikosta jokin toinen sivu.','dobbyts');?></p>
			<?php get_search_form(); ?>
		</article>
	</div>
</section>
<aside name="suggested-products">
	<div class="container">
		<h2><?php _e('Tutustu myös näihin','dobbyts');?></h2>
	</div>
	<div class="container">

		<?php 
			echo do_shortcode('[products limit=4 columns=4 orderby=popularity on_sale=false]');
		?>
	</div>
</aside>
