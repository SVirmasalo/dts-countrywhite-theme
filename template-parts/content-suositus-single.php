<?php
	/**
	* Variables
	**/
	if( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
		$thumbUrl = get_field('suosittelijan_kuva');
		$largeUrl = get_the_post_thumbnail_url(null,'large');
	}else{
		$thumbUrl = get_template_directory_uri().'/images/placeholder.jpg';
		$largeUrl = $thumbUrl;
	}
?>

<article <?php post_class();?> >
	<?php
	/**
	* SINGLE TEMPLATE FOR SINGLE
	*/
	?>
	<?php if(is_single()): ?>

		<figure role="banner">
			<img class="blog-hero" src="<?php echo $largeUrl;?>">
		</figure>
		<header class="single-article-header__testimonial">
			<blockquote><p><?php the_field('ingressi');?></p></blockquote>
		</header>
		<div class="single-article-content__testimonial">
			<?php the_field('sisalto');?>
		</div>
		<?php if(have_rows('suosikkituotteet')): ?>
			<div class="favourite-prods">
			<h3><?php _e('Suosikkituotteet');?></h3>
			<?php 
				$tuotteet = array();
				while(have_rows('suosikkituotteet')): the_row();
					$tuote = get_sub_field('tuote');
					array_push($tuotteet, $tuote->ID );

				endwhile;
				$subCols = count($tuotteet);
				echo '<div data-slide-count="'.$subCols.'">';
					echo do_shortcode( '[products ids="'.implode(",",$tuotteet).'" columns="'.$subCols.'" orderby="post__in" ]');
				echo '</div>';
			?>
			</div>
		<?php endif; ?>
		<footer class="single-article-footer__testimonial">
			<h2><?php _e('Kirjoittanut');?></h2>
			<figure>
				<?php
					$suosKuva = get_field('suosittelijan_kuva');
				?>
				<img src="<?php echo $suosKuva['sizes']['thumbnail']; ?>" alt="<?php _e('Flow Cosmetics suosittelija'); echo ' - '.get_field('suosittelijan_nimi'); ?> ">
				<figcaption>
					<p><?php the_field('suosittelijan_nimi'); ?></p>
					<p><a href="<?php the_field('linkki_ulkoiseen_sivustoon');?>"><?php the_field('ulkoisen_palvelun_nimi'); ?></a></p>
				</figcaption>

			</figure>
		</footer>
	<?php endif; ?>

	<?php
	/**
	* SINGLE TEMPLATE FOR ARCHIVE
	*/
	?>
	<?php if(is_archive()): ?>
		<header class="single-article-header__archive">
			<h2 class="single-article-title__archive"><?php the_title();?></h2>
		</header>
		<div class="single-article-content__archive">
			<p><?php the_field('ingressi');?></p>
			<a class="btn pink-tp" href="<?php the_permalink(); ?>"><?php _e('Read more','woocommerce');?></a>
		</div>
		<figure>
			<a href="<?php the_permalink(); ?>">
				<img src="<?php echo $thumbUrl['sizes']['shop_catalog']; ?>" alt="<?php the_title(); echo ' - '.__('Read more','woocommerce') ?>">
				<figcaption><i class="fa fa-link"></i></figcaption>
			</a>
		</figure>

	<?php endif; ?>
</article>