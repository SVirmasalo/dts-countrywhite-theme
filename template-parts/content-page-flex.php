<main>
	<header class="page-header">
		<div class="row">
		<div class="page-logo">
			<a href="<?php echo get_home_url(); ?>">
				<img src="<?php the_field('country_white_logo','option');?>" alt="Country White -verkkokaupan logo">
			</a>
		</div>
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('
			<p id="breadcrumbs">','</p>
			');
			}
		?>
		<h1 role="heading" class="site-title"><?php the_title(); ?></h1>
		</div>
		<div class="page-hero">
			<?php 
        	   	if ( has_post_thumbnail() ) {
					$hero_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
	           	}else{
	           		$hero_image_url = get_field('hero_default','option');
	           	}
			?>
			<div class="page-hero--image" <?php echo bgimg($hero_image_url); ?>>
				
			</div>
		</div>
	</header>
	<div class="flex-content-wrap">
		<?php 
		/**
		*	Sisäsivun joustava sisältö
		*/
		if(have_rows('page_content_flex_new')):
			while(have_rows('page_content_flex_new')): the_row();

				$rowLayout = get_row_layout();
				//var_dump($rowLayout);
				
				switch ($rowLayout) {
					case 'text_11':
							get_template_part('template-parts/flex-partial','texts');
						break;
					case 'text_05':
							get_template_part('template-parts/flex-partial','texts');
						break;
			
					case 'text_12':
							get_template_part('template-parts/flex-partial','texts');
						break;
			
					case 'text_13':
						get_template_part('template-parts/flex-partial','texts');
						break;
					case 'text_14':
							get_template_part('template-parts/flex-partial','texts');
						break;
					case 'map_full':
							get_template_part('template-parts/flex-partial','map-row');
						break;
					case 'map_12':
							get_template_part('template-parts/flex-partial','map-row');
						break;
					case 'row_products':
							get_template_part('template-parts/flex-partial','products-row');
						break;
					case 'row_brandit':
							get_template_part('template-parts/flex-partial','brands-row');
						break;
					
					default:
						# code...
						break;
				}

			endwhile;
		endif;

		?>
	</div>
</main>
