<?php 
/**
* This partial is for ACF-map rows
*/
?>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?php the_field('GoogleMaps_api_key','option');?>&callback=initMap" type="text/javascript"></script>
<script async defer type="text/javascript">

var initMap = function(){ // Callback for google maps api

	(function($) {

	function new_map( $el ) {
		
		// var
		var $markers = $el.find('.marker');
		
		// vars
		var args = {
			zoom		: 16,
			center		: new google.maps.LatLng(0, 0),
			mapTypeId	: google.maps.MapTypeId.ROADMAP
		};
		
		
		// create map	        	
		var map = new google.maps.Map( $el[0], args);
		
		
		// add a markers reference
		map.markers = [];
		
		
		// add markers
		$markers.each(function(){
			
	    	add_marker( $(this), map );
			
		});
		
		
		// center map
		center_map( map );
		
		
		// return
		return map;
		
	}

	function add_marker( $marker, map ) {

		// var
		var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

		// create marker
		var marker = new google.maps.Marker({
			position	: latlng,
			map			: map
		});

		// add to array
		map.markers.push( marker );

		// if marker contains HTML, add it to an infoWindow
		if( $marker.html() )
		{
			// create info window
			var infowindow = new google.maps.InfoWindow({
				content		: $marker.html()
			});

			// show info window when marker is clicked
			google.maps.event.addListener(marker, 'click', function() {
				// added in to close the previous open info window
				if($('.gm-style-iw').length) {
					$('.gm-style-iw').parent().hide();
	    		}

				infowindow.open( map, marker );

			});
		}

	}

	function center_map( map ) {

		// vars
		var bounds = new google.maps.LatLngBounds();

		// loop through all markers and create bounds
		$.each( map.markers, function( i, marker ){
			var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
			bounds.extend( latlng );
		});

		// only 1 marker?
		if( map.markers.length == 1 )
		{
			// set center of map
		    map.setCenter( bounds.getCenter() );
		    map.setZoom( 14 );
		}
		else
		{
			// fit to bounds
			map.fitBounds( bounds );
		}

	}

	var map = null;

	$(document).ready(function(){
		$('.acf-map').each(function(){
			map = new_map( $(this) );
		});

		setTimeout(function(e){
			console.log('timed out');
			google.maps.event.trigger(map, 'resize');
		}, 5000);
	});

	})(jQuery);
}
</script>







<?php 
	$rowStyle = get_row_layout(); 
	$bgcolor = get_sub_field('row_background');
	$raised = ( get_sub_field('raised_row') == 'yes' ? 'raised' : '' );
?>
<?php if( $rowStyle == 'map_full'):?>
	<div class="page-flex-row <?php echo get_row_layout() . ' ' . $bgcolor. ' ' . $raised; ?>">
		<div class="container">
			<?php
				$location = get_sub_field('map');
				$multilocation = ( have_rows('naytettavat_sijainnit') == true ? true : false );
				
				if($multilocation){

				?>
					<div class="acf-map">
						<?php while(have_rows('naytettavat_sijainnit')): the_row(); ?>
							<?php $location = get_sub_field('osoite');  ?>
							<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>" >
								<h1 class="marker-heading"><?php the_sub_field('sijainnin_nimi');?></h1>
								<p class="marker-description">
									<label class="marker-label" for="description"><?php _e('Kuvaus','dobby'); ?>:</label><br/>
									<span name="description"><?php the_sub_field('kohteen_kuvaus'); ?>
								</p>
								<label class="marker-label" for="address"><?php _e('Osoite','dobby'); ?>:</label><br/>
								<address name="address" class="marker-address"><?php echo $location["address"];?></address>
							</div>
						<?php endwhile;?>
					</div>
				<?php
				}
			?>
		</div>
	</div>
<?php endif; ?>

<?php if( $rowStyle == 'map_12'):?>
	<?php 
		$rowStyle = get_row_layout(); 
		$bgcolor = get_sub_field('row_background');
		$raised = ( get_sub_field('raised_row') == 'yes' ? 'raised' : '' );
	?>
	<div class="page-flex-row <?php echo get_row_layout() . ' ' . $bgcolor. ' ' . $raised; ?>">
		<div class="container">
			<div class="flex-cols equal">
				<div class="flex-col flex-col-1">
					<?php the_sub_field('sisalto');?>	
				</div>
				<div class="flex-col flex-col-2">
					<?php if(have_rows('naytettavat_sijainnit')): ?>
						<div class="acf-map">
							<?php while(have_rows('naytettavat_sijainnit')): the_row(); ?>
								<?php $location = get_sub_field('osoite');  ?>
								<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>" >
									<h1 class="marker-heading"><?php the_sub_field('sijainnin_nimi');?></h1>
									<p class="marker-description">
										<label class="marker-label" for="description"><?php _e('Kuvaus','dobby'); ?>:</label><br/>
										<span name="description"><?php the_sub_field('kohteen_kuvaus'); ?>
									</p>
									<label class="marker-label" for="address"><?php _e('Osoite','dobby'); ?>:</label><br/>
									<address name="address" class="marker-address"><?php echo $location["address"];?></address>
								</div>
							<?php endwhile;?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>