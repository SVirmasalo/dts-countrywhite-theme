<?php
/**
* Social media links
* You may want to extend them to work as sharing buttons
*/ 
?>
<div id="social-share" class="hide">
	<div class="socialbox">
		<?php if (have_rows('some_channels','option')): ?>
			<?php $sliCount = 0; ?>
			<ul class="socialbox-list">
			<?php while (have_rows('some_channels','option')):the_row(); ?>
				<?php $sliCount++; ?>
				<li class="socialbox-list--item sli-<?php echo $sliCount;?>"><a rel="me" title="<?php the_sub_field('channel_name');?>" target="_BLANK" href="<?php the_sub_field('channel_url');?>"><i class="fa <?php the_sub_field('channel_icon');?>"></i></a></li>
			<?php endwhile; ?>
			</ul>
		<?php endif; ?>
	</div>
</div>
