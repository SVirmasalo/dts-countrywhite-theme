<?php
	$orig_post = $post;
	$categories = get_the_category($orig_post->ID);

	$relatedArgs = array(
		'post_type' => 'post', 
		'posts_per_page' => 3,
		'exclude' => array($orig_post->ID),
		'category' => $categories[0]->term_id,
		'sortby' => 'date',
		);

	$relatedPosts = get_posts($relatedArgs);
	foreach ($relatedPosts as $relPost) {
		if(in_category('asiakassuositus')){
			$thumbUrl = get_field('suosittelijan_kuva',$relPost->ID);
			$thumbUrl = $thumbUrl['sizes']['shop_catalog'];
		}else{
			$thumbUrl = get_the_post_thumbnail_url($relPost->ID,'medium');
		}

		?>
		<article class="related-article">
			<header class="single-article-header__archive">
				<h2 class="single-article-title__archive"><?php echo $relPost->post_title; ?></h2>
			</header>
			<div class="single-article-content__archive">
				<p>
				<?php 
					if(in_category('asiakassuositus')){
						the_field('ingressi',$relPost->ID);
					}else{
						echo get_the_excerpt($relPost->ID);
					}
				?>
				</p>
				<a class="btn pink-tp" href="<?php the_permalink($relPost->ID); ?>"><?php _e('Read more','woocommerce');?></a>
			</div>
			<figure>
				<a href="<?php the_permalink(); ?>">
					<img src="<?php echo $thumbUrl; ?>" alt="<?php the_title(); echo ' - '.__('Read more','woocommerce') ?>">
					<figcaption><i class="fa fa-link"></i></figcaption>
				</a>
			</figure>
		</article>
		<?php
	}
	wp_reset_postdata();
?>