<?php 
/**
* Flexible content partial - 3 col content row
*/ 
$carousel = (get_sub_field('carousel') == 'true' ? 'carousel' : 'static');
$bgColor = (get_sub_field('taustavari') == 'white' ? 'white' : '');

?>
<section class="content-row cols-3 <?php echo $bgColor; ?>">
	<div class="container cols-<?php echo $carousel;?>">
		<div class="col col-1">
			<?php the_sub_field('col_1');?>
		</div>
		<div class="col col-3">
			<?php the_sub_field('col_2');?>
		</div>
		<div class="col col-4">
			<?php the_sub_field('col_3');?>
		</div>
	</div>
</section>
