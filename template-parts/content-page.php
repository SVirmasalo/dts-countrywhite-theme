<main>
	<header class="page-header">
		<div class="row">
			<div class="page-logo">
				<a href="<?php echo get_home_url(); ?>">
					<img src="<?php the_field('country_white_logo','option');?>" alt="Country White -verkkokaupan logo">
				</a>
			</div>
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('
				<p id="breadcrumbs">','</p>
				');
				}
			?>
			<h1 role="heading" class="site-title"><?php the_title(); ?></h1>
		</div>
		<div class="page-hero">
			<?php 
        	   	if ( has_post_thumbnail() ) {
					$hero_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
	           	}else{
	           		$hero_image_url = get_field('hero_default','option');
	           	}
			?>
			<div class="page-hero--image" <?php echo bgimg($hero_image_url); ?>>
				
			</div>
		</div>
	</header>
	<section class="primary-content">
		<div class="container">
			<article name="<?php global $post; echo $post->post_name; ?>" <?php post_class();?>>
				<?php the_content(); ?>
			</article>
		</div>
	</section>
</main>