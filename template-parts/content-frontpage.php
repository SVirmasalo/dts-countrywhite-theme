<?php 
	/**
	* Content for front page
	*/
	
?>
<main>
	<!--h1 class="site-title"><?php echo get_bloginfo('name');?></h1-->
	<div class="flex-content-wrap">
	<?php 
	/**
	*	Etusivun joustava sisältö
	*/
	if(have_rows('fp_content_flex')):
		while(have_rows('fp_content_flex')): the_row();

			$rowLayout = get_row_layout();

			switch ($rowLayout) {
				case 'lcl':
					get_template_part('template-parts/flex-partial','lcl');
					break;

				case 'shoprow_frontpage':
					get_template_part('template-parts/flex-partial','shoprow');
					break;
				
				case 'kuvarivi':
					get_template_part('template-parts/flex-partial','images-row');
					
					break;

				case 'brandit':
					get_template_part('template-parts/flex-partial','brands-row');
					break;

				case '3col_wysiwyg':

					get_template_part('template-parts/flex-partial','3col_wysiwyg');
					
					break;

				default:
					// Silncio!
					break;
			}
			
		endwhile;
	endif;

	?>
	</div>
</main>
<div id="herovideo-modal" aria-hidden="true" data-src="<?php the_field('hero_video_url');?>">
	<div id="player"></div>
	<!-- <span data-type="mp4"><?php the_field('hero_video_mp4');?></span>
	<span data-type="webm"><?php the_field('hero_video_webm');?></span>
	<span data-type="jpeg"><?php echo get_field('hero_poster');?></span>
	<button id="close-modal"><i class="fa fa-close"></i></button> -->
	<button id="close-video" target="herovideo-modal"><?php _e('Sulje video','dobby');?></button>
</div>