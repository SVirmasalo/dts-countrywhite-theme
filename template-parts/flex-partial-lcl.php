<?php 
/**
* Flexible content partial - Link/content/link (LCL)
*/ 

$bgColor = (get_sub_field('taustavari') == 'white' ? 'white' : '');

?>

<section class="content-row lcl <?php echo $bgColor; ?>">
	<div class="container lcl-cols">
		<div class="l-con left">
			<?php 
				$leftLink = get_sub_field('painike_vasen_kohde');
				$leftText = get_sub_field('painike_vasen_teksti');
			?>
			<a href="<?php the_permalink($leftLink);?>" class="btn sign">
				<?php echo $leftText; ?>
			</a>
		</div>
		<div class="c">
			<?php the_sub_field('tekstisisalto');?>
		</div>
		<div class="l-con right">
			<?php 
				$rightLink = get_sub_field('painike_oikea_kohde');
				$rightText = get_sub_field('painike_oikea_teksti');
			?>
			<a href="<?php the_permalink($rightLink);?>" class="btn sign">
				<?php echo $rightText; ?>
			</a>
		</div>
	</div>
</section>