<?php 
	$bgcolor = get_sub_field('row_background');
	$raised = ( get_sub_field('raised_row') == 'yes' ? 'raised' : '' );
	$desc = get_sub_field('products_description');
?>
<?php 

if (have_rows('products_cstm')):
	?>
	<div class="page-flex-row <?php echo 'shop-row' . ' ' . $bgcolor. ' ' . $raised; ?>">
		<div class="container">
			<div class="shop-row--heading">
				<?php echo $desc; ?>	
			</div>
			<?php
			$ids = '';
			while(have_rows('products_cstm')): the_row();
				$prod = get_sub_field('tuote');
				$ids .= $prod->ID.',';
			endwhile;
			?>
			<?php
				echo do_shortcode('[products ids="'.$ids.'"]');
			?>
		</div>
	</div>
	<?php
endif;
?>