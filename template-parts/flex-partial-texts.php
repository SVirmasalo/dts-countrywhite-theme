<?php
/**
* Partial to show flexible content of flex page
*/
?>

<?php 
	$rowStyle = get_row_layout(); 
	$bgcolor = get_sub_field('row_background');
	$raised = ( get_sub_field('raised_row') == 'yes' ? 'raised' : '' );
?>

<?php if( $rowStyle == 'text_11'):?>
	<div class="page-flex-row <?php echo get_row_layout() . ' ' . $bgcolor. ' ' . $raised; ?>">
		<div class="container">
			<?php the_sub_field('sisalto');?>
		</div>
	</div>
<?php endif; ?>

<?php if( $rowStyle == 'text_05'):?>
	<div class="page-flex-row half <?php echo get_row_layout() . ' ' . $bgcolor. ' ' . $raised; ?>">
		<div class="container">
			<div class="squeeze">
				<?php the_sub_field('sisalto');?>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php if( $rowStyle == 'text_12'):?>
	<div class="page-flex-row <?php echo get_row_layout() . ' ' . $bgcolor. ' ' . $raised; ?>">
		<div class="container">
			<div class="flex-cols <?php the_sub_field('flow');?>">
				<div class="flex-col flex-col-1">
					<?php the_sub_field('sisalto_vasen');?>	
				</div>
				<div class="flex-col flex-col-2">
					<?php the_sub_field('sisalto_oikea');?>	
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php if( $rowStyle == 'text_13'):?>
	<div class="page-flex-row <?php echo get_row_layout() . ' ' . $bgcolor. ' ' . $raised; ?>">
		<div class="container">
			<div class="flex-cols <?php the_sub_field('flow');?>">
				<div class="flex-col flex-col-1">
					<?php the_sub_field('sisalto_vasen');?>	
				</div>
				<div class="flex-col flex-col-2">
					<?php the_sub_field('sisalto_keski');?>	
				</div>
				<div class="flex-col flex-col-3">
					<?php the_sub_field('sisalto_oikea');?>	
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php if( $rowStyle == 'text_14'):?>
	<div class="page-flex-row <?php echo get_row_layout() . ' ' . $bgcolor. ' ' . $raised; ?>">
		<div class="container">
			<div class="flex-cols <?php the_sub_field('flow');?>">
				<div class="flex-col flex-col-1">
					<?php the_sub_field('sisalto_1');?>	
				</div>
				<div class="flex-col flex-col-2">
					<?php the_sub_field('sisalto_2');?>	
				</div>
				<div class="flex-col flex-col-3">
					<?php the_sub_field('sisalto_3');?>	
				</div>
				<div class="flex-col flex-col-4">
					<?php the_sub_field('sisalto_4');?>	
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>