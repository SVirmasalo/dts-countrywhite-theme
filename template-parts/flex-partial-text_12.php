<?php
/**
* Partial to show flexible content of flex page
*/
?>
<div class="page-flex-row <?php echo get_row_layout();?>">
	<div class="container">
		<div class="flex-cols <?php the_sub_field('flow');?>">
			<div class="flex-col flex-col-1">
				<?php the_sub_field('sisalto_vasen');?>	
			</div>
			<div class="flex-col flex-col-2">
				<?php the_sub_field('sisalto_oikea');?>	
			</div>
		</div>
	</div>
</div>