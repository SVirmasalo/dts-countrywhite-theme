<?php
	/**
	* Variables
	**/
	if( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
		$thumbUrl = get_the_post_thumbnail_url(null,'medium');
		$largeUrl = get_the_post_thumbnail_url(null,'large');
	}else{
		$thumbUrl = get_template_directory_uri().'/images/placeholder.jpg';
		$largeUrl = $thumbUrl;
	}
?>

<article <?php post_class();?> >
	<?php
	/**
	* SINGLE TEMPLATE FOR SINGLE
	*/
	?>
	<?php if(is_single()): ?>
		<p class="single-meta"><span name="meta-date"><?php the_time('j.m.y'); ?></span>-<span name="meta-author"><?php the_author(); ?></span></p>
		<figure role="banner">
			<img class="blog-hero" src="<?php echo $largeUrl;?>">
		</figure>
		<?php the_content(); ?>
	<?php endif; ?>

	<?php
	/**
	* SINGLE TEMPLATE FOR ARCHIVE
	*/
	?>
	<?php if(is_archive()): ?>
		<header class="single-article-header__archive">
			<h2 class="single-article-title__archive"><?php the_title();?></h2>
		</header>
		<div class="single-article-content__archive">
			<?php the_excerpt();?>
			<a class="btn pink-tp" href="<?php the_permalink(); ?>"><?php _e('Read more','woocommerce');?></a>
		</div>
		<figure>
			<a href="<?php the_permalink(); ?>">
				<img src="<?php echo $largeUrl; ?>" alt="<?php the_title(); echo ' - '.__('Read more','woocommerce') ?>">
				<figcaption><i class="fa fa-link"></i></figcaption>
			</a>
		</figure>

	<?php endif; ?>
</article>