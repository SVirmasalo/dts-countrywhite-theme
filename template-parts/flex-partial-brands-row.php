<?php 
/**
* Flexible content partial - brands row (as carousel)
*/ 
$bgColor = (get_sub_field('taustavari') == 'white' ? 'white' : '');

?>
<section class="content-row brands <?php echo $bgColor; ?>">
	<!-- <div class="container brand-row-content"><h2><?php _e('Brändit','dobby');?></h2></div>
	<div class="container-full">
		<?php 
			if(have_rows('brandit','option')):
				while(have_rows('brandit','option')): the_row();
					
						$brand = get_sub_field('brandi');
						$brandSite = get_sub_field('brand_site','option');
					?>
						<figure class="brand-container">
							<a href="">
								<img src="<?php echo $brand["sizes"]["thumbnail"];?>" alt="<?php echo $brand["alt"];?>" title="<?php echo $brand["description"]; ?>">
							</a>
						</figure>
					<?php
				endwhile;
			endif;
		?>
	</div>
	<div class="car-arrows"></div> -->
	<div class="container brand-row-content">
		<h2><?php _e('Brändit','dobby');?></h2>
	</div>
	<div class="container-full">
		<?php echo do_shortcode('[product_brand_thumbnails width="150" height="80" orderby="random" show_empty="false"]'); ?>
		<div class="slick-arrow-container">
			
		</div>
	</div>
</section>