<?php 
/**
* Flexible content partial - brands row (as carousel)
*/
?>
<section class="content-row images">
	<?php
		if (have_rows('kuvat')) {
			echo '<div class="images-wrap">';
			while (have_rows('kuvat')): the_row();
				$img = get_sub_field('kuva');
				$src = $img["sizes"]["large"];
				$thumb = $img["sizes"]["thumbnail"];
				// If explicit link has been set use that, but if not, use shop as a link
				$href = get_the_permalink( ( get_sub_field('linkki') != '' ? get_sub_field('linkki') : wc_get_page_id('shop')) );
				
				echo '<a href="'.$href.'">';
				echo '<img class="placeholder" data-src="'.$src.'" src="'.$thumb.'" alt="placeholder-image" />';
				echo '</a>';
			endwhile;
			echo '</div>';
		} // End repeater
	?>
</section>