<?php
/**
* Partial that pops up the contact form, enabled from theme settings
*/

?>
<a class="popup-with-form" href="#popup-form">
	<span id="tooltip-popup"><?php _e('Kysy','dobbyts');?>...</span>
	<span class="screen-reader-text"><?php _e('Kysy lisätietoja','dobbyts');?></span>
	<i class="fa fa-envelope"></i>
</a>

<div id="popup-form" class="mfp-hide white-popup-block">
	<?php
		/**
		* If code is not explicitly set, use one from theme settings
		*/
		if(get_field('valitse_lomake')){
			echo do_shortcode(get_field('valitse_lomake'));
		}else{
			echo do_shortcode(get_field('oletus_lomake','option'));
		}

	?>

</div>

<script type="text/javascript">
	/**
	* Script for popup - using magnific popup -plugin
	*/ 
	jQuery(document).ready(function() {

		//Insert current page url / slug to the certain field and hide it
		var url_field = document.getElementById('from-field');
		if(url_field.length != 0){
			console.log(window.location);
			url_field.value = window.location.toString(window.location.origin + window.location.pathname);
			url_field.hidden = true;
		}

		jQuery('a.popup-with-form').magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '.your-name',

			// When elemened is focused, some mobile browsers in some cases zoom in
			// It looks not nice, so we disable it:
			callbacks: {
				beforeOpen: function() {
					if(jQuery(window).width() < 700) {
						this.st.focus = false;
					} else {
						this.st.focus = '.your-name';
					}
				}
			}
		});
	});
</script>