<?php 
/**
* Flexible content partial - Shop loop row (Category or tags)
*/ 
?>
<?php $rowType = get_sub_field('ostatot_avainsanat'); ?>

<section class="content-row pop shop-show <?php echo $rowType; ?>">
	<div class="container shop-show-content">
		<h2><?php the_sub_field('otsake');?></h2>
		<?php 
			if(get_sub_field('alavirke')):
				echo '<p>'.get_sub_field('alavirke').'</p>';
			endif;
		?>

	</div>
	<div class="container">
		<?php 
			$rowType = get_sub_field('ostatot_avainsanat');
			switch ($rowType) {
				case 'category':
					$selectedCategories = get_sub_field('valitse_osastot');
					$maxColumns = sizeof($selectedCategories);
					$selectedCategories = implode(",", $selectedCategories);
					$shortCode = '[product_categories ids="'.$selectedCategories.'" columns="'.$maxColumns.'"]';
					echo do_shortcode($shortCode);

					break;

				case 'featured':
					echo do_shortcode('[featured_products limit="6" columns="6" orderby="rand" visibility="visible"]');
					break;

				case 'newly_added':
					echo do_shortcode('[products limit="6" columns="6" orderby="date" order="DESC" visibility="visible"]');
					break;

				case 'tag':
					$selectedTags = get_sub_field('valitse_avainsanat');
					$maxColumns = sizeof($selectedTags);
					echo '<div class="woocommerce columns-'.$maxColumns.'">';
						echo '<ul class="products" data-columns="'.$maxColumns.'">';
							foreach ($selectedTags as $tag ) {

								$tagData = dts_get_term($tag);
								//Testaukseen
								//var_dump($tagData);

								echo '<li class="product-category product">';
									echo '<a href="'.$tagData["permalink"].'" alt="">';
										echo '<img src="'.$tagData["termThumbUrl"].'">';
										echo '<h2 class="woocommerce-loop-category__title">'.$tagData["termName"].'</h2>';
									echo '</a>';
								echo '</li>';
							}
						echo '</ul>';
					echo '</div>';
					?>

					<?php
					
					break;
				default:
					//SILENCIO
					break;
			}
		?>
	</div>
</section>