<?php
/**
 * dobby the storekeeper functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 */
define( 'dobby_VERSION', '0.8.0' );
//define( 'CURLANG',_current_language());


/**
* Remove generator data
*/ 
function wpbeginner_remove_version() {
return '';
}
//add_filter('the_generator', 'wpbeginner_remove_version');

// ACF OPTIONS
include_once(__DIR__.'/lib/options.php');

// ADMIN EXTENSIONS


//require('lib/admin/custom_mce_buttons.php');
require('lib/admin/custom_mce_style.php');
//STYLE
function dobby_mce_styles() {
    add_editor_style( get_template_directory_uri().'/admin/mce.css' );
}
add_action( 'init', 'dobby_mce_styles' );


/**
 *  Set Yoast SEO plugin metabox priority to low
 */
function dobby_lowpriority_yoastseo() {
  return 'low';
}
add_filter( 'wpseo_metabox_prio', 'dobby_lowpriority_yoastseo' );

/**
 * Enable theme support for essential features
 */
load_theme_textdomain( 'dobby', get_template_directory() . '/languages');
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );
setlocale( LC_ALL, 'fi_FI.utf8' );


/**
* Custom media sizes
*/
add_image_size( 'brand-slider-size', 150, 80); // Soft crop (responsive)
//add_image_size( 'slider-size', 1440, 450, array( 'center', 'center' ) ); // Hard center
/*add_image_size( 'squared', 650, 650, array( 'center', 'center' ) ); // Hard center
add_image_size( 'squared-small', 250, 250, array( 'center', 'center' ) ); // Hard center
*/

/**
 * Editable navigation menus.
 */
register_nav_menus( array(
  'primary' => __( 'Primary Menu', 'dobby' ),
) );

/**
 * Remove WordPress Admin Bar when not on development env
 *
 * @link http://davidwalsh.name/remove-wordpress-admin-bar-css
 */
add_action( 'get_header', 'dobby_remove_admin_login_header' );
function dobby_remove_admin_login_header() {
  remove_action( 'wp_head', '_admin_bar_bump_cb' );
}
if ( getenv( 'WP_ENV' ) === 'development' && is_user_logged_in() ) {
  add_action('wp_head', 'dobby_dev_adminbar');
  function dobby_dev_adminbar() { ?>
    <style>
      html {
        height: auto;
        padding-bottom: 32px;
      }
      #wpadminbar {
        top: auto;
        bottom: 0;
      }
      #wpadminbar.nojs li:hover > .ab-sub-wrapper,
      #wpadminbar li.hover > .ab-sub-wrapper {
        bottom: 32px;
      }
    </style>
<?php }
} else {
  show_admin_bar(false);
}

/**
 * Enqueue scripts and styles.
 */
function dobby_scripts() {
  
  // If you want to use a different CSS per view, you can set it up here
  wp_enqueue_style( 'styles-main', get_template_directory_uri() . '/css/main.min.css' );
  wp_enqueue_style( 'styles-vendor', get_template_directory_uri() . '/css/vendor.min.css' );
  //wp_enqueue_style( 'styles-overwrite', get_template_directory_uri() . '/css/overwrite.css' );
  wp_enqueue_style( 'font-awesome','https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
  wp_enqueue_script( 'jquery-core' );
  //wp_enqueue_script( 'modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array('jquery'), dobby_VERSION, false );
  wp_enqueue_script( 'scripts-vendor', get_template_directory_uri() . '/js/vendor.min.js', array('jquery'), dobby_VERSION, true );
  wp_enqueue_script( 'scripts-main', get_template_directory_uri() . '/js/all.min.js', array(), dobby_VERSION, true );
// 
}
add_action( 'wp_enqueue_scripts', 'dobby_scripts' );


/**
===================================
===================================
********** WOOCOMMERCE*************
===================================
===================================
*/

// Declare woocommerce support
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

include_once(__DIR__.'/woocommerce/functions/settings.php');

function getPartsFromImgElString($img,$part){
  preg_match('/<img(.*)src(.*)=(.*)"(.*)"/U', $img, $result);
  $value = array_pop($result);
  var_dump($value);
  return $value;
}


// Get term link and random product image for it
function dts_get_term($term){

  $size = 'shop_catalog';
  
  $termObj = get_term($term);

  //var_dump( get_intermediate_image_sizes() );

  $termProds = get_posts(array(
    'posts_per_page' => 1,
    'post_type' => 'product',
    'orderby' =>'rand',
    'tax_query' => array(
      array(
        'taxonomy' => 'product_tag', 
        'field' => 'id',
        'terms' => $term,
      )
    )
  ));

  $prodImg = get_the_post_thumbnail_url($termProds[0]->ID, $size);

  //Placeholder if there's no product images
  if($prodImg == ''){
    $prodImg = wc_placeholder_img_src();
    //getPartsFromImgElString($prodImg,'src');
  }
  
  $termData = array(
    'permalink' => get_term_link($term),
    'termName' => $termObj->name,
    'termThumbUrl' => $prodImg,
  );

  return $termData;
}


// SIDEBAR WIDGET

/**
* Register our sidebars and widgetized areas.
*
*/
 
function woocommerce_widgets_custom() {

  register_sidebar( array(
    'name'          => 'Kauppasivun osastot',
    'id'            => 'woo_left_1',
    'before_widget' => '<div class="sidebar-container">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="sidebar-header">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => __('Brändit','dobby'),
    'id'            => 'brands_sidebar',
    'before_widget' => '<div class="sidebar-container">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="sidebar-header">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => __('Sivujen ja artikkeleiden vimpain','dobby'),
    'id'            => 'blog_sidebar',
    'before_widget' => '<div class="sidebar-col">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="sidebar-col--header">',
    'after_title'   => '</h3>',
  ) );

}
add_action( 'widgets_init', 'woocommerce_widgets_custom' );

// Cart to header
require_once( __DIR__. '/woocommerce/functions/cart-widget.php');




/*
add_action( 'init', 'custom_fix_thumbnail' );
 
function custom_fix_thumbnail() {
  add_filter('woocommerce_placeholder_img_src', 'custom_woocommerce_placeholder_img_src');
   
  function custom_woocommerce_placeholder_img_src( $src ) {
  $upload_dir = wp_upload_dir();
  $uploads = untrailingslashit( $upload_dir['baseurl'] );
  $src = __DIR__.'/images/placeholder.jpg';
   
  return $src;
  }
}
*/

// Login/account -links to header


function get_uacs(){
  if ( is_user_logged_in() ) { 
    echo '<a href="'.get_permalink(get_option('WooCommerce_myaccount_page_id')).'" title="'.__('My Account','woocommerce').'">'.__('My Account','woocommerce').'</a>'; 
    echo '<span>/</span>';
    echo '<a href="'.wp_logout_url(get_permalink()).'" title="'.__('logout','wordpress').'">'.__('logout','wordpress').'</a>';
  }else{
    echo '<a data-modal-target="login-form" href="#" title="'.__('Login','woocommerce').'">'.__('Login','woocommerce').'</a>';
    echo '&nbsp;/&nbsp;';
    echo '<a href="'.get_permalink(get_option('WooCommerce_myaccount_page_id')).'" title="'.__('Register','woocommerce').'">'.__('Register','woocommerce').'</a>';
  }
}


// Login form to footer-hook

function add_wc_login_to_footer(){
  if (! is_user_logged_in() ) { 
    include_once(__DIR__.'/woocommerce/functions/login-modal-widget.php');
  }
}
add_action('wp_footer','add_wc_login_to_footer');

/*

// Load scripts and styles only when necessary
add_action( 'wp_enqueue_scripts', 'crunchify_disable_woocommerce_loading_css_js' );
 
function crunchify_disable_woocommerce_loading_css_js() {
 
  // Check if WooCommerce plugin is active
  if( function_exists( 'is_woocommerce' ) ){
 
    // Check if it's any of WooCommerce page
    if(! is_woocommerce() && ! is_cart() && ! is_checkout() ) {     
      
      ## Dequeue WooCommerce styles
      wp_dequeue_style('woocommerce-layout'); 
      wp_dequeue_style('woocommerce-general'); 
      wp_dequeue_style('woocommerce-smallscreen');  
 
      ## Dequeue WooCommerce scripts
      wp_dequeue_script('wc-cart-fragments');
      wp_dequeue_script('woocommerce'); 
      wp_dequeue_script('wc-add-to-cart'); 
    
    }
  } 
}


// Remove additional information tab (S.Virmasalo)

/*
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] );   // Remove the additional information tab
    return $tabs;
}

// Re-order tabs
add_filter( 'woocommerce_product_tabs', 'woo_reorder_tabs', 98 );
function woo_reorder_tabs( $tabs ) {

  $tabs['reviews']['priority'] = 99;     // Reviews first
  $tabs['description']['priority'] = 10;      // Description second

  return $tabs;
}

// Add custom product tab called ingredients
/*
add_filter( 'woocommerce_product_tabs', 'woo_new_product_INCI_tab' );
function woo_new_product_INCI_tab( $tabs ) {
  
  // Adds the new tab
  
  $tabs['inci_tab'] = array(
    'title'   => __( 'Ingredients', 'dobby' ),
    'priority'  => 12,
    'callback'  => 'woo_new_product_tab_inci_content'
  );

  return $tabs;

}

function woo_new_product_tab_inci_content() {

  // The new tab content
  echo '<h2>'.__('Ingredients').'</h2>';
  $content = __('No additional information');
  //ACF Field if excists
  if(get_field('inci') != ''){
    $content = get_field('inci');
  }
  echo $content;
  
}

// Add custom product tab called Additional information

add_filter( 'woocommerce_product_tabs', 'woo_new_product_ADDINF_tab' );
function woo_new_product_ADDINF_tab( $tabs ) {
  
  // Adds the new tab
  
  $tabs['cadditional_information_tab'] = array(
    'title'   => __( 'Additional information', 'woocommerce' ),
    'priority'  => 11,
    'callback'  => 'woo_new_product_tab_addinfo_content'
  );

  return $tabs;

}
function woo_new_product_tab_addinfo_content() {

  // The new tab content
  echo '<h2>'.__('Additional information','woocommerce').'</h2>';
  $content = __('No additional information', 'woocommerce');
  // ACF Field
  if(get_field('lisatiedot_tuote') != ''){
    $content = get_field('lisatiedot_tuote');
  }
  echo $content;
  
}
*/

/**
* HELPERS
*/ 
function bgimg($url){
  return "style=background-image:url('${url}');";
}
function makeBtn($url,$class,$desc){
  return "<a href='${url}' class='btn ${class}'>${desc}</a>";
}

function getRandomImages($times){
  if($times > 5){
    $times = 5;
  }
  $base = 'http://svirmasalo.dev/countrywhite.fi/wp-content/uploads/2017/';
  $imgs = [
    '11/sea-of-fog-fog-ocean-sea-52514.jpeg',
    '11/pexels-photo-262367.jpeg',
    '11/pexels-photo-672655.jpeg',
    '11/pexels-photo-236028.jpeg',
    '11/pexels-photo-207732.jpeg',
  ];

  for ($i=0; $i < $times; $i++) { 
    echo '<img class="placeholder" src="'.$base.$imgs[$i].'" alt="placeholder-image" />';
  }
}

