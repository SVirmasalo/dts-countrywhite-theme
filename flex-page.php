<?php // template name: joustava
/**
* The template for the flexible sub page
*
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
*/
get_header(); 

?>
<?php
	if ( have_posts() ) :
		/* Start the Loop */
		while ( have_posts() ) : the_post();
			get_template_part('template-parts/content','page-flex');
		endwhile;
		//the_posts_navigation();
	else :
		get_template_part( 'template-parts/content', 'none' );
	endif; 
?>
<?php
	get_footer();
?>