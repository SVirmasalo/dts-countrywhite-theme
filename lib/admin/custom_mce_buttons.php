<?php 

	add_shortcode('clear','mce_clear');
	function mce_clear($atts){
		$html = '<div class="clear"></div>';
		return $html;
	}
	add_shortcode('lineBreak','mce_lineBreak');
	function mce_lineBreak($atts){
		$html = '<br class="lineBreak/>';
		return $html;
	}
	add_shortcode('button','mce_dobby_button');
	function mce_dobby_button($atts){
		$html = '<a class="mce-btn"></a>';
		return $html;
	}

	add_action('init','custom_mce_buttons');
	function custom_mce_buttons(){
		add_filter("mce_external_plugins","custom_mce_add_buttons");
		add_filter('mce_buttons','custom_mce_register_buttons');
	}
	function custom_mce_add_buttons(){
		$plugin_array['customButtons'] = get_stylesheet_directory_uri().'/lib/admin/custom-mce-plugin.js';
		return $plugin_array;
	}
	function custom_mce_register_buttons($buttons){
		array_push($buttons,'clear','lineBreak','button');
		return $buttons;
	}

?>