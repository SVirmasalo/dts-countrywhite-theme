<?php
/**
* Optins page
*/
$optionsArgs_top = array(
	'page_title' => 'Teeman asetukset',
	'menu_title' => 'Teeman asetukset',
	'menu_slug' => 'teeman-asetukset',
	'redirect' => false,
);
$optionsArgs_products = array(
	'page_title' 	=> 'Tuote-sivujen asetukset',
	'menu_title'	=> 'Tuote-sivut',
	'parent_slug'	=> 'teeman-asetukset',
);
$optionsArgs_header = array(
	'page_title' 	=> 'Teeman yläosan asetukset',
	'menu_title'	=> 'Header',
	'parent_slug'	=> 'teeman-asetukset',
);
$optionsArgs_footer = array(
	'page_title' 	=> 'Teeman alaosan asetukset',
	'menu_title'	=> 'Footer',
	'parent_slug'	=> 'teeman-asetukset',
);

if( function_exists('acf_add_options_page') ) {

  	acf_add_options_page( $optionsArgs_top );
  	acf_add_options_sub_page($optionsArgs_products);
  	acf_add_options_sub_page($optionsArgs_header);
  	acf_add_options_sub_page($optionsArgs_footer);

}

// Init Maps API-key
function dobby_acf_init() {

	$GM_apiKey = get_field('GoogleMaps_api_key','option');
	acf_update_setting('google_api_key', $GM_apiKey);

}

// Init Some-buttons
function someBar(){
	$site_wide_setting = get_field('some_toggler','option');
	$cur_page_setting = get_field('some_toggler_single');

	if(is_null($cur_page_setting)){
		$cur_page_setting = true;
	}
	
	if($site_wide_setting === true && $cur_page_setting != false){
		// Insert somes here
		get_template_part('template-parts/partial','some');		
	}/*else{
		// Don't insert somes
	}*/
	

}
add_action('wp_footer','someBar');

add_action('acf/init', 'dobby_acf_init');

?>