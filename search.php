<?php
/**
 * The main template file for pages
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */
get_header(); ?>
<main>
	<!-- <header class="page-header">
		<h1 role="heading" class="site-title"><?php _e('Haun tulos'); ?></h1>
	</header> -->
	<header class="page-header">
		<div class="page-logo">
			<a href="<?php echo get_home_url(); ?>">
				<img src="<?php the_field('country_white_logo','option');?>" alt="Country White -verkkokaupan logo">
			</a>
		</div>
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('
			<p id="breadcrumbs">','</p>
			');
			}
		?>
		<h1 role="heading" class="site-title"><?php _e('Haun tulos'); ?></h1>
		</div>
		<div class="page-hero">
			<?php 
				$hero_image_url = get_field('hero_default','option');
			?>
			<div class="page-hero--image" <?php echo bgimg($hero_image_url); ?>>
				
			</div>
		</div>
	</header>
	<section class="primary-content blog">
		<div class="container">
		<?php
			if ( have_posts() ) :
				/* Start the Loop */
				while ( have_posts() ) : the_post();
					?>
						<article <?php post_class();?> >
							<?php 
							/**
							* Variables
							**/
							if( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
								$thumbUrl = get_the_post_thumbnail_url(null,'shop_thumbnail');
								$largeUrl = get_the_post_thumbnail_url(null,'shop_thumbnail');
							}else{
								$thumbUrl = get_template_directory_uri().'/images/placeholder.jpg';
								$largeUrl = $thumbUrl;
							}
							
							?>
							<figure class="search-thumbnail">
								<img src="<?php echo $thumbUrl; ?>" class="search-thumbnail">
							</figure>
							<div class="search-content">
								<?php the_title('<h2 class="small">','</h2>'); ?>
								<?php the_excerpt(); ?>		
								<a class="btn" title="<?php the_title();?>" href="<?php the_permalink();?>"><?php _e('Read more','woocommerce');?></a>
							</div>
						</article>
					<?php
				endwhile;
				//the_posts_navigation();
			else :
				get_template_part( 'template-parts/content', 'none' );
			endif; 
		?>
		</div>
	</section>
<?php
	get_footer();
?>