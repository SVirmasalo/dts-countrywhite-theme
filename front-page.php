<?php // template name: etusivu
	/**
	 * The template for frontpage of our theme
	 *
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
	 *
	 */
	get_header(); 
?>

<?php
	if ( have_posts() ) {
		?>
			<section id="fp_carousel" role="banner">
				
			</section>

		<?php
		/* Start the Loop */
		while ( have_posts() ) : the_post();
			get_template_part('template-parts/content','frontpage');
		endwhile;
	}else {
		echo '404!';
		//get_template_part( 'template-parts/content', 'none' );
	}; 
?>
<?php
	get_footer();
?>