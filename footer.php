<?php
/**
 * The template for displaying the footer
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
<div class="tie">
	<p><?php the_field('slogan','option');?></p>
</div>
<footer class="site-footer">
	<div class="site-info">
		<div class="container">
			<div class="flex-wrap">
			<?php 
				if(have_rows('footer','option')):
					while (have_rows('footer','option')) : the_row();
						echo '<div class="site-info_col">';
							the_sub_field('sisaltoalue');
						echo '</div>';
					endwhile;
				endif;
			?>
			</div>
		</div>
	</div>
	<div class="site-meta">
		<p> copyright <?php echo bloginfo( 'name' ); echo ' '; echo date('Y'); ?> - Design by Drumsö Brand Machine</p>
	</div>
</footer>
<button id="scrollTop">
	<i class="fa fa-angle-up"></i>
</button>
<?php 
	if(get_field('contact_popup')):
		get_template_part('template-parts/partial','modal-contact');
	endif;
?>


<?php wp_footer(); ?>

</body>
</html>