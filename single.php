<?php
/**
 * The main template file for pages
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */
get_header(); ?>
<main>
	<header class="page-header">
		<div class="page-logo">
			<a href="<?php echo get_home_url(); ?>">
				<img src="<?php the_field('country_white_logo','option');?>" alt="Country White -verkkokaupan logo">
			</a>
		</div>
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('
			<p id="breadcrumbs">','</p>
			');
			}
		?>
		<h1 role="heading" class="site-title"><?php the_title();?></h1>
	</header>
	<section class="primary-content">
		<div class="container">
		<?php $weHavePosts = false; ?>
		<?php
			if ( have_posts() ) :
				/* Start the Loop */
				while ( have_posts() ) : the_post();
					get_template_part('template-parts/content','single');

				endwhile;
				the_posts_navigation();
			else :
				get_template_part( 'template-parts/content', 'none' );
			endif; 
		?>
		</div>
		<div class="container">
			<?php get_sidebar('blog'); ?>
		</div>
	</section>
	<aside role="complementary" class="related-posts--single">
		<!-- Related posts  -->
		<?php 
			$curPostID = get_the_ID(get_post());
			$relatedArgs = array(
				'post_type' => 'post',
				'exclude' => $curPostID,
				'post_per_page' => 2
			);
			$relatedPosts = get_posts($relatedArgs);

			if($relatedPosts):
			?>
			
			<div class="container">
				<h1 class="small"><?php _e('Lisää luettavaa', 'dobbyts');?></h1>
				<div class="related-posts-wrapper">
				<?php
				foreach ($relatedPosts as $rpost) {
					$att_src = wp_get_attachment_url( get_post_thumbnail_id( $rpost->ID ) );
					echo "<article class='related-post post'>";
					echo "<a href=".get_permalink($rpost->ID).">";
					echo "<header class='related-post--header' ".bgImg($att_src).">";
					//echo "<img src=".$att_src." alt='' />";
					echo "</header>";
					echo "<div class='related-post--content'>";
					echo "<h2 class='small'>".$rpost->post_title."</h2>";
					echo "</div>";
					echo "</a>";
					echo "</article>";
				}
				?>
				</div>
			</div>
			
			<?php
			endif;


		?>
	</aside>
</main>
<?php
	get_footer();
?>