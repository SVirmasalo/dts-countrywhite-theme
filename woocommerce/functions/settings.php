<?php
/**
* Some extra settings and functions
*/ 
 

add_action( 'init', 'custom_fix_thumbnail' );
 
function custom_fix_thumbnail() {

  	add_filter('woocommerce_placeholder_img_src', 'custom_woocommerce_placeholder_img_src');
   
	function custom_woocommerce_placeholder_img_src( $src ) {
		if(get_field('oletuskuva_uusille_tuotteille','option')){
			$img = get_field('oletuskuva_uusille_tuotteille','option');
			$src = $img["sizes"]["shop_catalog"];
			return $src;
		}else{
			return $src;
		}
	}
}


?>