<?php
	/**
	* Login modal
	*/
?>
<div class="modal" data-modal-name="login-form" aria-hidden="true" role="dialog" aria-labelby="loginTitle" aria-describedby="loginDescription">
	<div class="modal-container">
		<header>
			<h2 id="loginTitle"><?php _e('Login','woocommerce');?></h2>
			<button data-modal-target="login-form"><i class="fa fa-close"></i></button>
		</header>
		<div class="modal-content">
			<p id="loginDescription"><?php _e('Please enter your credentials to login','dobby');?></p>
			<?php 
				echo woocommerce_login_form();
			?>
		</div>
	</div>
</div>