<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

	<main class="woocommerce-page">
    <header class="page-header woocommerce-products-header">
    	<div class="page-logo">
			<a href="<?php echo get_home_url(); ?>">
				<img src="<?php the_field('country_white_logo','option');?>" alt="Country White -verkkokaupan logo">
			</a>
		</div>
    	<?php
			if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('
			<p id="breadcrumbs">','</p>
			');
			}
		?>
		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h1 role="heading" class="site-title woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>

		<?php endif; ?>


    </header>
    	<div class="container">
    		<?php
				/**
				 * woocommerce_archive_description hook.
				 *
				 * @hooked woocommerce_taxonomy_archive_description - 10
				 * @hooked woocommerce_product_archive_description - 10
				 */

				do_action( 'woocommerce_archive_description' );
			?>
    	</div>
    	<div class="wrap">
    	<?php if( ! is_tax('brand') ){ ?> 
			<aside class="category-sidebar">
				<div class="container" name="category-sidebar">
					<?php
						/**
						 * woocommerce_sidebar hook.
						 *
						 * @hooked woocommerce_get_sidebar - 10
						 */
						do_action( 'woocommerce_sidebar' );
						dynamic_sidebar( 'brands_sidebar' ); 
					?>
				</div> <!-- CONTAINER -->
			</aside>
		<?php } ?>
	    <section class="primary-content" data-list="grid">
		    <div class="container">

				<?php if ( have_posts() ) : ?>


					<div class="displaying-options" name="displaying-options" aria-hidden="true">
						<button data-target="displaying-options"><i class="fa fa-list"></i></button>
						<div class="catalog-info">
							<?php
								/**
								 * woocommerce_before_shop_loop hook.
								 *
								 * @hooked wc_print_notices - 10
								 * @hooked woocommerce_result_count - 20
								 * @hooked woocommerce_catalog_ordering - 30
								 */
								do_action( 'woocommerce_before_shop_loop' );
							?>
						</div>
						<div class="catalog-controls">
							<ul calss="view-switch-list">
								<li><button id="toggleList"><i class="fa fa-list-ul"></i></button></li>
								<li><button id="toggleGrid" class="active"><i class="fa fa-th"></i></button></li>
							</ul>
						</div>
					</div>

					<?php woocommerce_product_loop_start(); ?>

						<?php woocommerce_product_subcategories(); ?>

						<?php while ( have_posts() ) : the_post(); ?>

							<?php
								/**
								 * woocommerce_shop_loop hook.
								 *
								 * @hooked WC_Structured_Data::generate_product_data() - 10
								 */
								//var_dump(get_the_ID());
								//update_post_meta( get_the_ID(), '_thumbnail_id', 65 );
								//add_filter('loop_shop_columns', 'loop_columns');
								if (!function_exists('loop_columns')) {
									function loop_columns() {
										return 3; // 3 products per row
									}
								}
								do_action( 'woocommerce_shop_loop' );
							?>

							<?php wc_get_template_part( 'content', 'product' ); ?>

						<?php endwhile; // end of the loop. ?>

					<?php woocommerce_product_loop_end(); ?>

					<?php
						/**
						 * woocommerce_after_shop_loop hook.
						 *
						 * @hooked woocommerce_pagination - 10
						 */
						do_action( 'woocommerce_after_shop_loop' );
					?>

				<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

					<?php
						/**
						 * woocommerce_no_products_found hook.
						 *
						 * @hooked wc_no_products_found - 10
						 */
						do_action( 'woocommerce_no_products_found' );
					?>

				<?php endif; ?>

			<?php
				/**
				 * woocommerce_after_main_content hook.
				 *
				 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
				 */
				//do_action( 'woocommerce_after_main_content' );
			?>
			<!--</div>  CONTAINER -->
		</section>
		<?php
			get_template_part('template-parts/flex-partial','brands-row');
		?>
		
		</div> <!-- WRAP -->
	</main>

<?php get_footer( 'shop' ); ?>
