<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */
get_header(); ?>

<main>
	<header class="page-header">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('
			<p id="breadcrumbs">','</p>
			');
			}
		?>
		<h1 role="heading" class="site-title"><?php echo single_cat_title(); ?></h1>
	</header>
	<section class="primary-content blog">
		<div class="container">
			<?php if ( have_posts() ) :
					/* Start the Loop */
					$weHavePosts = true;
					while ( have_posts() ) : the_post();
						/*if(!in_category('asiakassuositus')){
							
						}else{
							
						}*/
						get_template_part('template-parts/content','single');
						//get_template_part('template-parts/content','suositus-single');
					endwhile;
					the_posts_navigation();
				else :
					get_template_part( 'template-parts/content', 'none' );
				endif; 
			?>
		</div>
	</section>
</main>
<?php
	get_footer();
?>