<?php
/**
* Sidebar template for blogs, posts and pages
*/
?>
<aside class="post-sidebar">
	<?php if ( is_active_sidebar( 'blog_sidebar' ) ) : ?>
		<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
			<?php dynamic_sidebar( 'blog_sidebar' ); ?>
		</div><!-- #primary-sidebar -->
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery('#primary-sidebar').attr('data-length', jQuery('#primary-sidebar .sidebar-col').length );
			});
		</script>
	<?php endif; ?>
</aside>