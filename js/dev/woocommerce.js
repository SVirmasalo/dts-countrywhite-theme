(function($) {

	/**
	/*Local variables
	*/
	var winWidth = $(window).width();
	//const MOBILE_BREAKPOINT = 769;


	/**
	/*Event functions
	*/

	//toggle displaying options
	function toggleDisplayOptions(){
		const panel = $('.displaying-options');
		if(panel.attr('aria-hidden') === 'true'){
			panel.attr('aria-hidden','false');
			$('body').addClass('lock');
		}else{
			panel.attr('aria-hidden','true');
			$('body').removeClass('lock');
		}
	}

	//Switch view
	function switchProductListView(viewType){
		const viewArea = $('*[data-list]');
		if(viewType === 'list'){
			viewArea.attr('data-list','list');
		}else if(viewType === 'grid'){
			viewArea.attr('data-list','grid');
		}else{
			viewArea.attr('data-list','grid');
		}
	}

	/**
	/*Event handlers
	*/


	//init buttons if is archive
	const toggler_displayOptions = $('button[data-target="displaying-options"]');
	const toggler_listView = document.getElementById('toggleList');
	const toggler_gridView = document.getElementById('toggleGrid');
	function initButtonsForArchive(){
		toggler_displayOptions.on('click',function(e){
			e.stopPropagation();
			toggleDisplayOptions();
		});
		toggler_listView.addEventListener('click',function(e){
			e.stopPropagation();
			$(this).addClass('active');
			$(toggler_gridView).removeClass('active');
			switchProductListView('list');
		});
		toggler_gridView.addEventListener('click',function(e){
			e.stopPropagation();
			$(this).addClass('active');
			$(toggler_listView).removeClass('active');
			switchProductListView('grid');
		});
	}

	/*
	* MAGNIFIC POPUP
	*/
	//Find all a-tags with image inside
	function initMagnificPopup(){
		const imgContainer = $('.woocommerce-product-gallery__wrapper');
		imgContainer.magnificPopup({
			delegate:'a',
			type:'image',
			tLoading: 'Ladataan kuvaa #%curr%...',
			mainClass:'mfp-img-mobile',
			gallery:{
				enabled:true,
				navigateByImgClick:true,
				preload:[1]
			},
			image:{
				tError: 'Kuvaa ei voitu ladata.',
				titleSrc: function(item){
					//console.log(item.el[0].firstChild.attributes[3].value);
					return /*item.el[0].firstChild.attributes[3].value +*/ '<small>© Country White</small>';
				}
			}
		})
	}

	$(document).ready(function(){
		//WHEN IN ARCHIVE
		if($('body').hasClass('archive') && $('body').hasClass('woocommerce')){
			initButtonsForArchive();
		}
		//WHEN IN SINGLE PRODUCT
		if($('body').hasClass('single-product')){
			initMagnificPopup();
		}
	});

})( jQuery );