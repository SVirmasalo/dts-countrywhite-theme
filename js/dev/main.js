'use strict';

(function($) {

	/**
	* Events
	*/

	// Clean empty nodes
	function clean(node)
	{
	  for(var n = 0; n < node.childNodes.length; n ++)
	  {
	    var child = node.childNodes[n];
	    if
	    (
	      child.nodeType === 8 
	      || 
	      (child.nodeType === 3 && !/\S/.test(child.nodeValue))
	    )
	    {
	      node.removeChild(child);
	      n --;
	    }
	    else if(child.nodeType === 1)
	    {
	      clean(child);
	    }
	  }
	}
	clean(document.body);
	


	// Variables based on events
	var searchInput = document.querySelector('input[type="search"]');
	var winWidth = $(window).width();
	const nav = document.querySelector('nav[role="navigation"]');
	const allSubMenus = new Array();
	const MOBILE_BREAKPOINT = 769;
	const sTop = document.getElementById('scrollTop');
	const socialShare = document.getElementById('social-share');

	if($('.popup-with-form').length === 0){
		sTop.style.bottom = '2.3rem';
	}
	
	function toggleShowHide(){
		try{
			var tama = $(this);
			if(tama.hasClass('show')){
				tama.toggleClass('show').toggleClass('hide');
				tama.attr('aria-hidden','true');
			}else if(tama.hasClass('hide')){
				tama.toggleClass('hide').toggleClass('show');
				tama.attr('aria-hidden','false');
			}else{
				console.log('something wrong here..');
			}
		}catch(error){
			console.log('social share bar caused an error:', err);
		}
	}

	// Event functions

	// Scroll top
	var sTopEvent = {
		state : false,
		setState : function(newState){
			this.state = newState;
		}
	}

	$(window).on('scroll', function(e){

		var offset = 300;
		
		if($(this).scrollTop() > offset && sTopEvent.state === false){
			$(sTop).animate({
				opacity:1,
			},248);
			toggleShowHide.call(socialShare);
			sTopEvent.setState(true);
		}
		if($(this).scrollTop() < offset && sTopEvent.state === true){
			$(sTop).animate({
				opacity:0,
			},248);
			toggleShowHide.call(socialShare);
			sTopEvent.setState(false);
		}
	});

	function toggleSearch(obj){
		$('.search-items').toggleClass('show-sbox');	
		searchInput.autofocus = true;
		searchInput.autocomplete = true;
		searchInput.cheked = true;

		if($(obj).attr('aria-visible') == 'false'){
			//$(obj).attr('aria-hidden','true');
			$(obj).attr('aria-visible','true');
		}else{
			//$(obj).attr('aria-hidden','false');
			$(obj).attr('aria-visible','false');
		}
	}

	function nav_openclose(){
		var navState = nav.getAttribute('aria-hidden');
		if(navState === 'true'){
			nav.setAttribute('aria-hidden','false');
			$('body').addClass('lock');
		}else{
			nav.setAttribute('aria-hidden','true');
			$('body').removeClass('lock');
		}
	}

	var toggleSubMenu = function(){
		$(this).toggleClass('active');
		var target = '#'+this.dataset.target + ' .sub-menu';
		$(target).toggleClass('open');
	}

	function toggleLoginModal(){
		var modal = $('.modal[data-modal-name="login-form"]');
		var modal_state = modal.attr('aria-hidden');
		if(modal_state === 'true'){
			modal.attr('aria-hidden','false');
		}else{
			modal.attr('aria-hidden','true');
		}
	}
	function toggleSubscribeModal(){
		var modal = $('.modal[data-modal-name="subscribe-form"]');
		var modal_state = modal.attr('aria-hidden');
		if(modal_state === 'true'){
			modal.attr('aria-hidden','false');
		}else{
			modal.attr('aria-hidden','true');
		}
	}
	var catsToDownBar = {
		state :false,
		og : $('.sidebar-container'),
		temp :  $('.sidebar-container').clone(),
		containersWrap : document.createElement('DIV'),
		catCarSettings: {
			slidesToShow:1,
			autoplay:false,
			accessibility:true,
			adaptiveHeight:false,
			arrows:true,
		},
		add : function(){
			this.state = true;
			this.temp.appendTo(this.containersWrap);
			$(this.containersWrap).addClass('scroller').appendTo('.displaying-options').slick(this.catCarSettings);

			console.log('cats added');
		},
		remove : function(){
			this.state = false;
			this.temp.remove();
			$(this.containersWrap).slick('unslick');
			this.containersWrap.remove();
			console.log('cats removed');
		}

	}

	

	// Wievport events
	if(winWidth >= MOBILE_BREAKPOINT){
		//$(nav).removeAttr('aria-hidden');
	}
	if(winWidth < MOBILE_BREAKPOINT){
		if( $('body').hasClass('archive','woocommerce') ){
			catsToDownBar.add();
		}
	}
	
	/**
	* WATCH WINDOW.RESIZE AT THE END OF THIS DOCUMENT!!!!
	*/ 

	/**
	* Event handlers
	*/

	// Scroll top anchor
	sTop.addEventListener('click',function(e){
		e.preventDefault();
		if (sTopEvent.state){
			$('body,html').animate({
				scrollTop:0
			},800);
			return false;
		}else{

		}
	})

	//Mobile nav
	var navTogglers = document.querySelectorAll('button[data-target="main-nav"]');
	for(var sel = 0; sel < navTogglers.length; sel++){
		navTogglers[sel].addEventListener('click',function(e){
			e.stopPropagation();
			nav_openclose();
		});
	}	

	//Login modal
	var loginTogglers = document.querySelectorAll('*[data-modal-target="login-form"]');
	for(var i_lt = 0; i_lt < loginTogglers.length; i_lt++){
		loginTogglers[i_lt].addEventListener('click',function(e){
			e.preventDefault();
			toggleLoginModal();
		});
	}

	//Subscribe modal
	var subscribeTogglers = document.querySelectorAll('*[data-modal-target="subscribe-form"]');
	for(var i_lt = 0; i_lt < subscribeTogglers.length; i_lt++){
		subscribeTogglers[i_lt].addEventListener('click',function(e){
			e.preventDefault();
			toggleSubscribeModal();
		});
	}

	$('.control-panel .container').on('scroll',function(e){
		e.preventDefault();
		var sleft = this.scrollLeft;

		
		$('.control-panel').addClass('touch');
		if(sleft < 10){
			setTimeout(function(){
				$('.control-panel').removeClass('touch');
			},2500);
		}
	});;

	//Prevent focusing
	$('.slick-slide a').on('focus',function(e){
		console.log(e);
	})
	$('.slick-slide a').on('blur',function(e){
		console.log(e);
	})

	/**
	* Custom Objects
	*/
	function SST(id){
		this.id = id;
		this.button = document.createElement('BUTTON');
		this.button.dataset.target = this.id;
		this.button.className = 'sub-menu-toggle';
		this.button.innerHTML = '<i class="fa fa-chevron-left"></i>';
		this.button.onclick = toggleSubMenu;
		return this.button;
	}
	/**
	* DOM manipulations
	*/
	//Find all sub menus from main-menu
	$('.menu-item-has-children').each(function(k,v){
		//var tempSST = ;
		$(v).prepend(new SST(v.id));
	});

	
	//Slick frontpage hero carousel
	const fp_car = $('section.brands .container-full ul.brand-thumbnails');
	fp_car.slick({
		infinite:true,
		slidesToShow:6,
		autoplay:true,
		autoplaySpeed:5000,
		accessibility:true,
		pauseOnHover:true,
		adaptiveHeight:false,
		dots:false,
		arrows:true,
		appendArrows: '.slick-arrow-container',
		centeredMode: true,
		responsive:[
			{
				breakpoint: 990,
				settings: {
					slidesToShow:4,
					autoplaySpeed: 4000,
				}
			},
			{
				breakpoint: MOBILE_BREAKPOINT,
				settings: {
					slidesToShow:3,
					autoplaySpeed: 3500,
				}
			},
			{
				breakpoint: 560,
				settings: {
					slidesToShow:2,
					autoplaySpeed: 3500,
				}
			},
			{
				breakpoint: 360,
				settings: {
					slidesToShow:1,
					autoplaySpeed: 3500,
				}
			}
		]

	});

	//Slick frontpage hero carousel
	const cols3 = $('section.cols-3 .cols-carousel');
	cols3.slick({
		infinite:true,
		slidesToShow:3,
		autoplay:false,
		accessibility:true,
		adaptiveHeight:false,
		dots:false,
		arrows:true,
		centeredMode: true,
		responsive:[
			{
				breakpoint: MOBILE_BREAKPOINT,
				settings: {
					slidesToShow:1,
					autoplay:true,
					autoplaySpeed: 3500,
					arrows:true,
				}
			}
		]

	});

	/**
	* Only if smaller than mobile breakpoint
	*/ 
	const shopCar = $('section.shop-show.tag .products');
	if(winWidth < MOBILE_BREAKPOINT){
		shopCar.slick({
			infinite:false,
			accessibility: true,
			slidesToShow:2,
			autoplay:true,
			autoplaySpeed:3500,
			centered:false,
			infinite:false,
			dots:true,
			arrows:false,
		});
	}


	/**
	* FLEX SHOP CAROUSEL
	*/ 
	const flexShopCar = $('.page-flex-row.shop-row ul.products');
	if(winWidth < MOBILE_BREAKPOINT){
		flexShopCar.slick({
			infinite:false,
			accessibility:true,
			adaptiveHeight:false,
			slidesToShow:2,
			autoplay:false,
			autoplaySpeed:4000,
			arrows:false,
			dots:true,
			responsive:[
				{
					breakpoint:480,
					settings:{
						slidesToShow:1,
					}
				}
			]
		});
	}

	/*
	* MAGNIFIC POPUP
	*/
	//Find all a-tags with image inside
	function initArticleGallery(){
		$('div.gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		closeOnContentClick: false,
		closeBtnInside: false,
		mainClass: 'mfp-with-zoom mfp-img-mobile',
		image: {
			verticalFit: true,
			titleSrc: function(item) {
				var desc = $(item.el.context.firstChild).attr('aria-describedby');
				if(desc){
					return document.getElementById(desc).innerText;
				}else{
					return '';
				}
			}	
		},
		gallery: {
			enabled: true
		},
		zoom: {
			enabled: true,
			duration: 300, // don't foget to change the duration also in CSS
			opener: function(element) {
				return element.find('img');
			}
		}
		
		});
	}

	function initPageGallery(){
		$('article .gallery').magnificPopup({
			delegate:'a',
			type:'image',
			closeOnContentClick:false,
			closeBtnInside:false,
			mainClass: 'mfp-with-zoom mfp-img-mobile',
			image: {
				verticalFit: true,
				titleSrc: function(item) {
					var desc = $(item.el.context.firstChild).attr('aria-describedby');
					if(desc){
						return document.getElementById(desc).innerText;
					}else{
						return '';
					}
				}	
			},
			gallery: {
				enabled: true
			},
			zoom: {
				enabled: true,
				duration: 300, // don't foget to change the duration also in CSS
				opener: function(element) {
					return element.find('img');
				}
			}
			
			});
	}

	if($('article').hasClass('post')){
		initArticleGallery();
	}
	if($('body').hasClass('page-template-default')){
		initPageGallery();
	}

	var imageRows = [];

	function imagesRow_imgsToBG(row){
		
		var images = $(this).find('.placeholder');
		for (var i = 0; i < images.length; i++) {
			var image = images[i];
			var imageParent = image.parentElement;
			imageParent.style.backgroundImage = 'url('+image.dataset.src+')';
			image.style.opacity = 0;
		}
	}
	function imagesRow_height(row){
		var images = $(this).find('.placeholder');
		var rowWidth = this.context.clientWidth;
		var wantedHeight = ( ( (rowWidth / images.length) / 4 ) * 3 );
		this.context.style.height = wantedHeight+'px';
	}

	function imageRowsModify(rows){
		for (var i = 0; i < this.length; i++) {
			imagesRow_height.call(this[i]);
		}
	}

	function equalizeHeights(listOfObjects){
		/**
		* Equalizes the height of its children
		*/

		try{
			var eachSel = $(this.selector+'  > *');
			var maxH = 0;
			eachSel.each(function(k,v){
				console.log($(v).height());
				if( $(v).height() > maxH ){
					maxH = $(v).height();
				}
			});
			eachSel.height(maxH);

		}catch(err){
			console.log('equalizeHeights caused an error: ', err);
		}
	}
	// equalizeHeights.call($('ul.products'));

	$(document).ready(function(){
		var cpanel_h = $('.control-panel').height();
		
		$('header.site-header').css('top',cpanel_h);

		$(".content-row.images .images-wrap").each(function(){
			imagesRow_imgsToBG.call($(this));
			imagesRow_height.call($(this));
			imageRows.push($(this));
		});

		/**
		* HIGHLIGHT IMAGES
		*/
	
			const imagesToHighlight = $('body.home .content-row.shop-show .product img');
			var iteratorPerse = 0;
			const MAX_ITEMS = imagesToHighlight.length;
			const MAX_LAPS = 4;
			var laps = 0;
			const ROUTINE_TIME = 1500; 
			if(MAX_ITEMS > 0){
				var highLightRoutine = setInterval(function(){
					highlightPorcess();	
				},ROUTINE_TIME);
			}

			function scale(){
				var tama = $(this);
				tama.toggleClass('scale');
				setTimeout(function(){
					tama.toggleClass('scale');
				},ROUTINE_TIME);

			}

			function highlightPorcess(){

				scale.call( imagesToHighlight[iteratorPerse] );
				
				if(iteratorPerse < MAX_ITEMS){
					iteratorPerse++;
				}else{
					if(laps < MAX_LAPS){
						iteratorPerse = 0;
						laps++;
					}else{
						clearInterval(highLightRoutine);
					}
				}
			}




	});	

	$(window).resize(function(){
		$('body').removeClass('lock');

		imageRowsModify.call(imageRows);


		winWidth = $(window).width();
		if(winWidth >= MOBILE_BREAKPOINT){
			//$(nav).removeAttr('aria-hidden');

			if($('body').hasClass('archive','woocommerce') && catsToDownBar.state === true){
				catsToDownBar.remove();
				
			}

		}else{
			//$(nav).attr('aria-hidden','true');
			if($('body').hasClass('archive','woocommerce') && catsToDownBar.state === false){
				catsToDownBar.add();

			}
		}	
	});

	
	
})( jQuery );