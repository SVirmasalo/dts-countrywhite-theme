( function( $ ) {

	if($('#herovideo-modal').length != 0){

		// Vars
		var playBtn = document.getElementById('play-full-video'),
			closeBtn = document.getElementById('close-video'),
			videoModal = document.getElementById('herovideo-modal'),
			firstLoad = true;

		/** Video init **/
		var tag = document.createElement('script');
		tag.id = 'youtube';
		tag.src = 'https://www.youtube.com/iframe_api';

		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag,firstScriptTag);

		var player;
		var playerDimensions = {
			width: ( $(window).width() > 990 ? '896' : '640') ,
			height: ( $(window).height() > 576 ? '540' : '390') ,
		}

		var videoId = videoModal.dataset.src.split('/');
		videoId = videoId[videoId.length -1];

		function onYouTubeIframeAPIReady() {
			console.log('ontuberedy');
	        player = new YT.Player('player', {
				height: playerDimensions.height,
				width: playerDimensions.width,
				videoId:videoId,
				events: {
				'onReady': onPlayerReady,
				'onStateChange': onPlayerStateChange
				}
	        });
  		}

		function onPlayerReady(event) {
			event.target.playVideo();
		}
		var done = false;

		function onPlayerStateChange(event) {
			if (event.data == YT.PlayerState.PLAYING && !done) {
				done = true;
			}
		}
		function pauseVideo() {
			console.log('stoppin');
			player.pauseVideo();	
		}


		/** Events **/

		//Open video
		playBtn.addEventListener('click',function(e){
			e.stopPropagation();
			videoModal.className = "show";
			videoModal.setAttribute('aria-hidden', 'false');

			// Only init video once
			if(firstLoad === true){
				onYouTubeIframeAPIReady();
				firstLoad = false;
			}
		});

		closeBtn.addEventListener('click',function(e){
			e.stopPropagation();
			
			pauseVideo();

			videoModal.className = "";
			videoModal.setAttribute('aria-hidden', 'true');
		});
		


		console.log('herovideo available');

	    
	    if($('body').hasClass('possibly-touch')){
	        function testVideoPresense(){
	            setTimeout(function(){
	                if($('video').length != 0){
	                    $('video').attr({'playsinline':'','webkit-playsinline':''});
	                }else{
	                    console.log('not found yet');
	                    testVideoPresense();
	                }
	            },300);
	        }
	        testVideoPresense();
	    };
	}

} )( jQuery );