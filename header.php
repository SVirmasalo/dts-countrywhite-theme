<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri();?>/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri();?>/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri();?>/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_stylesheet_directory_uri();?>/images/favicon/manifest.json">
	<link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri();?>/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">


	
	<?php $google = get_field('ga_code','option'); ?>

	<?php if($google): ?>
		<?php 
			$GSRC = 'https://www.googletagmanager.com/gtag/js?id='.$google;
			$CONFIG = $google;
		?>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', '<?php echo $CONFIG; ?>');
		</script>

	<?php endif; ?>

	<?php wp_head(); ?>
</head>
<body <?php body_class();?>>

<aside class="control-panel">
	<div class="container">
		<?php
			/*$langArgs = array(
					'show_flags' => 1,
					'show_names' => 0
				);
			*/
		?>
		<!-- <ul class="lang-switch">FI | EN </ul> -->
		<ul class="some-list">
			<li><a href=""><i class="fa fa-facebook"></i></a></li>
			<li><a href=""><i class="fa fa-instagram"></i></a></li>
		</ul>
		<div class="account">
			<?php get_uacs(); ?>
		</div>
		<div class="cart">
			<a class="cart-customlocation" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php echo sprintf ( _n( '%d '.__('item'), '%d '.__('items'), WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?>&nbsp;/&nbsp;<?php echo WC()->cart->get_cart_total(); ?></a>
			<i class="fa fa-shopping-cart"></i>
		</div>
		<div class="search">
			<div aria-visible="true" class="search-box">
				<?php get_search_form();?>
			</div>
		</div>
	</div>
</aside>

<!-- NAVIGATION -->

<header class="site-header">
	<div class="container">
		<button data-target="main-nav"><i class="fa fa-bars"></i></button>
		<nav role="navigation" aria-hidden="true">
			<?php 
				$menuArgs = array(
					'container' => 'div',
					'echo' => true,
				);
				wp_nav_menu($menuArgs);
			?>
			<button data-target="main-nav"><i class="fa fa-close"></i></button>
		</nav>
	</div>
</header>




<?php if(is_front_page()):?>
	<div id="hero-vid" 
		data-vide-bg="poster:<?php echo get_field('hero_poster');?>"
		data-vide-options="loop:true, muted:true, position:50% 50%, posterType:detect">

		<div class="container">
			<div class="hero-content-area">
				<figure>
					<a href="" title="Country White - Etusivu">
						<img src="<?php the_field('country_white_logo','option');?>" alt="Country White -verkkokaupan logo">
					</a>
					<figcaption>
						<h1><?php the_field('hero_punchline');?></h1>
					</figcaption>
				</figure>
				<div class="hero-controls">
					<button id="play-full-video"><i class="fa fa-play"></i></button>
					<p><?php the_field('hero_video_description','option');?></p>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php
	// Breadcrumbs
	/*/if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('
	<p id="breadcrumbs">','</p>
	');
	}*/
?>