<?php
/**
 * The template for displaying the footer of shop pages
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>

<?php 
/**
* Set content before footer
*/
if(is_single()):
	$global_lcl = get_field('lcl_before_footer','option');
	if($global_lcl):
	?>
	<div class="flex-content-wrap">
		<section class="content-row lcl white">
			<div class="container lcl-cols">
				<div class="l-con left">
					<?php 
						$leftLink = $global_lcl['painike_vasen_kohde'];
						$leftText = $global_lcl['painike_vasen_teksti'];
					?>
					<a href="<?php the_permalink($leftLink); ;?>" class="btn sign">
						<?php echo $leftText; ?>
					</a>
				</div>
				<div class="c">
					<?php echo $global_lcl['tekstisisalto']; ?>
				</div>
				<div class="l-con right">
					<?php 
						$rightLink = $global_lcl['painike_oikea_kohde'];
						$rightText = $global_lcl['painike_oikea_teksti'];
					?>
					<a href="<?php the_permalink($rightLink); ;?>" class="btn sign">
						<?php echo $rightText; ?>
					</a>
				</div>
			</div>
		</section>
	</div>
	<?php
	endif;
endif;
?>
<div class="tie">
	<p><?php the_field('slogan','option');?></p>
</div>
<footer class="site-footer">
	<div class="site-info">
		<div class="container">
			<div class="flex-wrap">
			<?php 
				if(have_rows('footer','option')):
					while (have_rows('footer','option')) : the_row();
						echo '<div class="site-info_col">';
							the_sub_field('sisaltoalue');
						echo '</div>';
					endwhile;
				endif;
			?>
			</div>
		</div>
	</div>
	<div class="site-meta">
		<p> copyright <?php echo bloginfo( 'name' ); echo ' '; echo date('Y'); ?> - Design by Drumsö Brand Machine</p>
	</div>
</footer>
<button id="scrollTop">
	<i class="fa fa-angle-up"></i>
</button>
<?php 
	if(get_field('contact_popup')):
		get_template_part('template-parts/partial','modal-contact');
	endif;
?>
<?php wp_footer(); ?>

</body>
</html>