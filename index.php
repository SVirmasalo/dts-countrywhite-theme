<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */
get_header(); ?>

<?php
	if ( have_posts() ) :
		if ( is_home() && ! is_front_page() ) : ?>
			<header>
				<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
			</header>

		<?php
		endif;
		/* Start the Loop */
		while ( have_posts() ) : the_post();
			/*
			 * Include the Post-Format-specific template for the content.
			 * If you want to override this in a child theme, then include a file
			 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
			 */
			the_title('<h1>','</h1>');
		endwhile;
		the_posts_navigation();
	else :
		?>
		<header class="page-header">
			<div class="page-logo">
				<a href="<?php echo get_home_url(); ?>">
					<img src="<?php the_field('country_white_logo','option');?>" alt="Country White -verkkokaupan logo">
				</a>
			</div>
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('
				<p id="breadcrumbs">','</p>
				');
				}
			?>
			<h1 role="heading" class="site-title">404</h1>
		</header>
		<?php
		echo '<main>';
			get_template_part( 'template-parts/content', 'none' );
		echo '</main>';
	endif; 
?>
<?php
	get_footer();
?>