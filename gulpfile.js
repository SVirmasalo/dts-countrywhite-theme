/*JS VARIABLES*/
const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps');
const uglify      = require('gulp-uglify');
const concat = require('gulp-concat');
const rename = require('gulp-rename');

/*STYLE VARIABLES*/
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const minifycss = require('gulp-clean-css');

/*MEDIA*/
const imagemin = require('gulp-imagemin');


/* PATH VARIABLES */

//JS
const jsSrc = 'js/dev/*.js';
const jsDist = 'js';
const conditionizr = './vendor/conditionizr/dist/conditionizr.min.js';
const slick = './vendor/slick/slick/slick.min.js';
//const scrollMagic = './vendor/scroll-magic/scrollmagic/minified/ScrollMagic.min.js';
const magnificPopup = './vendor/magnific-popup/jquery.magnific-popup.min.js';
const vide = './vendor/vide/jquery.vide.min.js';
//const wpss = 'js/vendor/wordpress-social-share/wpss.legacy.js';

//STYLES
const scssSrc = 'scss/**/*.{scss,sass}';
const scssGlobalSrc = 'scss/main.scss';
const cssDist = 'css';
const slickStyle = './vendor/slick/slick/slick.css';
const slickTheme = './vendor/slick/slick/slick-theme.css';
const magnificPopupStyle = './vendor/magnific-popup/magnific-popup.css';

//IMAGEMIN


/*
UGLIFY JAVASCRIPT
========================================
*/

gulp.task('uglify-main',function(){
	return gulp.src([jsSrc])
		.pipe(sourcemaps.init())
        .pipe(concat('all.min.js'))
		.pipe(uglify({preserveComments: false, compress: true, mangle: true}).on('error',function(e){console.log('\x07',e.message);return this.end();}))
		//.pipe(rename({ extname: '.min.js' }))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(jsDist))
    .pipe(browserSync.reload({stream:true}))
});

gulp.task('uglify-vendor',function(){
    return gulp.src([slick,vide,magnificPopup, conditionizr])
        .pipe(concat('vendor.min.js'))
        .pipe(uglify({preserveComments: false, compress: true, mangle: true}).on('error',function(e){console.log('\x07',e.message);return this.end();}))
        //.pipe(rename({ extname: '.min.js' }))
        .pipe(gulp.dest(jsDist))
    .pipe(browserSync.reload({stream:true}))
});

/*
PREFIX. CONCAT AND MINIFY STYLES
================================
*/

gulp.task('styles-main', () =>
    gulp.src(scssGlobalSrc)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error',sass.logError))
		.pipe(autoprefixer({
            browsers:['> 2%','last 3 versions']
        })) // Adds browser prefixes (eg. -webkit, -moz, etc.)
        //.pipe(concat('styles.min.css'))
        .pipe(minifycss({compatibility: 'ie8'},{debug:true}, function(details) {
        	console.log(details.name + ': ' + details.stats.originalSize);
        	console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
        .pipe(rename({ extname: '.min.css' }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(cssDist))
        .pipe(browserSync.reload({stream:true}))
);

gulp.task('styles-vendor', () =>
    gulp.src([slickStyle,slickTheme,magnificPopupStyle])
        .pipe(sass().on('error',sass.logError))
        .pipe(concat('vendor.min.css'))
        .pipe(minifycss({compatibility: 'ie8'},{debug:true}, function(details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
        .pipe(gulp.dest(cssDist))
        .pipe(browserSync.reload({stream:true}))
);


/*
IMAGEMIN
========
*/
gulp.task('imagemin',() =>
    gulp.src('../../uploads/2017/**/*.{jpg,jpeg,png,svg,gif}')
    .pipe(imagemin())
    .pipe(gulp.dest('../../uploads/2017/'))
);

/*
BROWSERSYNC STUFF
=================
*/
gulp.task('browser-sync',function(){
    browserSync.init({
        open:'external',
        host:'svirmasalo.dev',
        proxy:{
            target: "svirmasalo.dev/countrywhite.fi"
        },
        port:8080
    })
});




/*
WATCH
=====
*/
gulp.task('js-watch', ['uglify-main'/*,'uglify-vendor'*/]);

gulp.task('watch', ['browser-sync'], function() {
    gulp.watch(["./*.php","./template-parts/*.php","./woocommerce/**/*.php"]).on('change',browserSync.reload);
    gulp.watch(scssSrc, ['styles-main'/*,'styles-vendor'*/]).on('change',browserSync.reload);
    gulp.watch(jsSrc, ['js-watch']).on('change',browserSync.reload);
});

/**
WATCH IMAGES
============
*/
gulp.task('watch-images',function(){
    gulp.watch('../../uploads/2017/**/*.{jpg,jpeg,png,svg,gif}');
})